# Jāgaro's GnuPG / YubiKey / SSH Guide

[Back to Overview](README.md).

The following presumes you already [created the master keys](master.md).

## Copy GnuPG Sub-Keys to YubiKeys

### 1. Copy Master GnuPG Directory

The transfer of sub-keys to the YubiKey (step 4) will destroy data in the `gnupg-master`. Instead, we create and use a temporary copy:

```sh
GNUPG_TEMP_DIR="$WORK_DIR/gnupg-temp"
rm -rf "$GNUPG_TEMP_DIR"
cp -a "$GNUPG_MASTER_DIR" "$GNUPG_TEMP_DIR"

# Important!
export GNUPGHOME="$GNUPG_TEMP_DIR"
```

### 2. Configure YubiKey with `gpg`

Insert a YubiKey. Then:

```sh
gpg --card-edit
```

| Prompt                    | Answer           | Meaning                                    |
| ------------------------- | ---------------- | ------------------------------------------ |
| gpg/card>                 | admin            | Access admin function                      |
| gpg/card>                 | kdf-setup        | Pass hashes of PINs (Admin PIN `12345678`) |
| gpg/card>                 | passwd           | Edit PINs                                  |
| Your selection?           | 1                | Change (User) PIN (default `123456`)       |
| Your selection?           | 3                | Change Admin PIN (default `12345678`)      |
| Your selection?           | Q                | Return to card editor                      |
| gpg/card>                 | name             | Change name                                |
| Cardholder's surname      | Last             |
| Cardholder's given name   | First            |
| gpg/card>                 | lang             | Change language                            |
| Language preferences      | en               |
| gpg/card>                 | login            | Change login                               |
| Login data (account name) | user@example.com |
| gpg/card>                 | quit             |

### 3. Configure YubiKey with `ykman`

```sh
ykman openpgp access set-retries 10 10 10
```

### 4. Transfer Sub-Keys to YubiKey

```sh
gpg --edit-key $KEY_ID
```

| Prompt                         | Answer    | Meaning                            |
| ------------------------------ | --------- | ---------------------------------- |
| gpg>                           | key 1     | Select signing sub-key             |
| gpg>                           | keytocard | Move sub-key to YubiKey            |
| ...where to store...selection? | 1         | Signature key slot on YubiKey      |
| gpg>                           | key 1     | Deselect signing sub-key           |
| gpg>                           | key 2     | Select encryption sub-key          |
| gpg>                           | keytocard | Move sub-key to YubiKey            |
| ...where to store...selection? | 2         | Encryption key slot on YubiKey     |
| gpg>                           | key 2     | Deselect signing sub-key           |
| gpg>                           | key 3     | Select authentication sub-key      |
| gpg>                           | keytocard | Move sub-key to YubiKey            |
| ...where to store...selection? | 3         | Authentication key slot on YubiKey |
| gpg>                           | save      | Confirm and make changes           |

### 5. Verify

```sh
gpg --list-secret-keys
```

Confirm all three sub-keys have lines that start with `ssb>` which indicate they have moved to the YubiKey.

### 6. Cleanup

```sh
rm -rf "$GNUPG_TEMP_DIR"
```

### 5. Repeat as Necessary

Repeat steps 1-4 for each YubiKey.
