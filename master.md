# Jāgaro's GnuPG / YubiKey / SSH Guide

[Back to Overview](README.md).

## Create Master GnuPG Keys

### 1. Computer / VM

Choose a reasonably secure environment. Ideally, one without network access.
Ensure the following packages are available:

```sh
sudo apt-get install -y gnupg scdaemon yubikey-manager
```

Disconnect network afterwards.

### 2. Setup Environment

```sh
killall gpg-agent

WORK_DIR="$HOME/gnupg-yubikey-ssh"
GNUPG_MASTER_DIR="$WORK_DIR/gnupg-master"

mkdir -p "$GNUPG_MASTER_DIR"
chmod 700 "$GNUPG_MASTER_DIR"
cat >"$GNUPG_MASTER_DIR/gpg.conf" <<EOF
keyid-format 0xlong
with-subkey-fingerprints
personal-cipher-preferences AES256 AES192 AES
personal-digest-preferences SHA512 SHA384 SHA256
personal-compress-preferences ZLIB BZIP2 ZIP Uncompressed
default-preference-list SHA512 SHA384 SHA256 AES256 AES192 AES ZLIB BZIP2 ZIP Uncompressed
cipher-algo AES256
cert-digest-algo SHA512
s2k-cipher-algo AES256
s2k-digest-algo SHA512
keyserver hkps://keys.openpgp.org
display-charset utf-8
EOF

cd "$WORK_DIR"
export GNUPGHOME="$GNUPG_MASTER_DIR"
```

### 3. Create a Master Password

```sh
gpg --gen-random 2 22 | base64 | \
  sed -r 's/^(.{6})(.{6})(.{6})(.{6})(.{6}).*/\1.\2.\3.\4.\5/g'
```

**_Write this down!_**

### 4. Create the Certification (Master) Key

```sh
gpg --expert --full-generate-key
```

| Prompt                               | Answer           | Meaning                          |
| ------------------------------------ | ---------------- | -------------------------------- |
| ...what kind of key...selection?     | 11               | ECC (set your own capabilities)  |
| Possible actions...selection?        | S                | Disable Sign                     |
| Possible actions...selection?        | Q                | Confirm Certify only             |
| ...which elliptic curve...selection? | 1                | Curve 25519                      |
| Key is valid for?                    | 0                | Does not expire                  |
| Is this correct?                     | y                |                                  |
| Real name                            | First Last       |                                  |
| Email address                        | user@example.com |                                  |
| Comment                              |                  | No comment needed                |
| Change...or (O)kay/(Q)uit?           | O                | Confirm create certification key |

Enter the master password when prompted.

### 5. Assign `KEY_ID` Variable

After the certification key is created, you should see output as follows:

```
...
pub   ed25519/0x1234567890ABCDEF 2023-01-01 [C]
      ABCDEF1234567890ABCDEF1234567890ABCDEF12
uid                              First Last <user@example.com>
```

Assign the key ID (line after `pub`) to the `$KEY_ID` variable:

```sh
KEY_ID=ABCDEF1234567890ABCDEF1234567890ABCDEF12
```

### 6. Create Sub-Keys

#### 6a. Create the Signing Sub-Key

```sh
gpg --expert --edit-key $KEY_ID
```

| Prompt                               | Answer | Meaning                |
| ------------------------------------ | ------ | ---------------------- |
| gpg>                                 | addkey | Add a new sub-key      |
| ...what kind of key...selection?     | 10     | ECC (sign only)        |
| ...which elliptic curve...selection? | 1      | Curve 25519            |
| Key is valid for?                    | 3y     | 3 years                |
| Is this correct?                     | y      | Confirm                |
| Really create?                       | y      | Confirm create sub-key |

#### 6b. Create the Encryption Sub-Key

| Prompt                               | Answer | Meaning                |
| ------------------------------------ | ------ | ---------------------- |
| gpg>                                 | addkey | Add a new sub-key      |
| ...what kind of key...selection?     | 12     | ECC (encrypt only)     |
| ...which elliptic curve...selection? | 1      | Curve 25519            |
| Key is valid for?                    | 3y     | 3 years                |
| Is this correct?                     | y      | Confirm                |
| Really create?                       | y      | Confirm create sub-key |

#### 6c. Create the Authentication (SSH) Sub-Key

| Prompt                               | Answer | Meaning                         |
| ------------------------------------ | ------ | ------------------------------- |
| gpg>                                 | addkey | Add a new sub-key               |
| ...what kind of key...selection?     | 11     | ECC (set your own capabilities) |
| Possible actions...selection?        | S      | Disable Sign                    |
| Possible actions...selection?        | A      | Enable Authenticate             |
| Possible actions...selection?        | Q      | Confirm Authenticate only       |
| ...which elliptic curve...selection? | 1      | Curve 25519                     |
| Key is valid for?                    | 3y     | 3 years                         |
| Is this correct?                     | y      | Confirm                         |
| Really create?                       | y      | Confirm create sub-key          |
| gpg>                                 | save   |                                 |

### 7. Verify Keys

#### 7a. List Keys

```sh
gpg --list-keys
gpg --list-secret-keys
```

#### 7b. Test Keys

```sh
killall gpg-agent
echo 'billy bob' | gpg --armor --sign --encrypt \
  --default-key $KEY_ID --recipient $KEY_ID --output test-encrypt.asc
killall gpg-agent
gpg --decrypt test-encrypt.asc
```

### 8. Create Backup

#### 8a. Export Public and Secret Keys

```sh
BACKUP_DIR="$(pwd)/gnupg-master-$(date +%Y%m%d)"
mkdir -p "$BACKUP_DIR"
chmod 700 "$BACKUP_DIR"
gpg --armor --output "$BACKUP_DIR/public.key" --export $KEY_ID
gpg --armor --output "$BACKUP_DIR/master.key" --export-secret-keys $KEY_ID
gpg --armor --output "$BACKUP_DIR/subs.key" --export-secret-subkeys $KEY_ID
```

#### 8b. Export Revocation Certificate

```sh
gpg --output "$BACKUP_DIR/revoke.asc" --gen-revoke $KEY_ID
```

| Prompt                                   | Answer       | Meaning                  |
| ---------------------------------------- | ------------ | ------------------------ |
| ...Create a revocation certificate...    | y            |                          |
| ...reason for the revocation...decision? | 1            | Key has been compromised |
| ...optional description...               | Lost access! |                          |
| Is this okay? (y/N)                      | y            | Confirm                  |

#### 8c. Create Tarball

```sh
tar -cz -C "$(dirname "$BACKUP_DIR")" -f "$BACKUP_DIR.tar.gz" \
  "$(basename "$BACKUP_DIR")"
gpg --symmetric "$BACKUP_DIR.tar.gz"
shred -u "$BACKUP_DIR.tar.gz" "$BACKUP_DIR"/*
rmdir "$BACKUP_DIR"
```

#### 8d. Copy Tarball

Copy the tarball to a USB drive or other places for safe keeping.

### 9. Test Backup

On another computer or VM without network access, copy the encrypted tarball and
`test-encrypt.asc` to it. Then:

```
gpg --decrypt --use-embedded-filename gnupg-master-*.tar.gz.gpg
tar -xzf gnupg-master-*.tar.gz
gpg --import gnupg-master-*/master.key
gpg --import gnupg-master-*/subs.key
gpg --import gnupg-master-*/public.key
gpg --decrypt test-encrypt.asc
```
