# Renew Subkeys

TODO: Cleanup, etc...

## Prepare

```sh
EXPIRATION="2030-01-01"

GNUPG_MASTER_DIR="$(pwd)/gnupg-master"
WORK_DIR="$(pwd)/work"
mkdir -p "$WORK_DIR"

GNUPG_TEMP_DIR="$WORK_DIR/gnupg-master-temp"
rm -rf "$GNUPG_TEMP_DIR"
cp -a "$GNUPG_MASTER_DIR" "$GNUPG_TEMP_DIR"

# Important!
export GNUPGHOME="$GNUPG_TEMP_DIR"

KEY_IDS=($(
  gpg -K --with-colons | grep ^fpr | awk -F: '{ print $10 }'
))
KEY_ID="${KEY_IDS[0]}"
SUBKEY_IDS=("${KEY_IDS[@]:1}")
```

## Verify

```sh
echo "GNUPGHOME=$GNUPGHOME"
echo "KEY_ID=$KEY_ID"
echo "SUBKEY_IDS=${SUBKEY_IDS[@]}"
echo "EXPIRATION=$EXPIRATION"
```

## Renew!

```sh
echo -e 'trust\n5\ny\nquit' | gpg --command-fd=0 --edit-key "$KEY_ID"

BACKUP_DIR="$WORK_DIR/gnupg-master-backup-$(date +%Y%m%d)"
mkdir -p "$BACKUP_DIR"
chmod 700 "$BACKUP_DIR"

gpg --armor --output "$BACKUP_DIR/old-public.key" --export "$KEY_ID"

pkill gpg-agent
for SUBKEY_ID in "${SUBKEY_IDS[@]}"; do
  gpg --quick-set-expire "$KEY_ID" "$EXPIRATION" "$SUBKEY_ID"
done

gpg --armor --output "$BACKUP_DIR/public.key" --export "$KEY_ID"
gpg --armor --output "$BACKUP_DIR/master.key" --export-secret-keys "$KEY_ID"
gpg --armor --output "$BACKUP_DIR/subs.key" --export-secret-subkeys "$KEY_ID"
echo -e 'y\n1\nLost access!\n\ny' |
  gpg --command-fd=0 --output "$BACKUP_DIR/revoke.asc" --gen-revoke "$KEY_ID"
```

## Update an Existing GnuPG Keystore and Update Key Server

On a fresh terminal:

```sh
KEY_ID=...
gpg --import path/to/new/public.key
echo -e 'trust\n5\ny\nquit' | gpg --command-fd=0 --edit-key "$KEY_ID"
gpg --keyserver hkps://keys.openpgp.org --send "$KEY_ID"
```

## Update Remaining GnuPG KeyStores

```sh
gpg --refresh-keys
```
