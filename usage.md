# Jāgaro's GnuPG / YubiKey / SSH Guide

[Back to Overview](README.md).

## General Usage

### First Time

#### 1. Install Packages

```sh
sudo apt-get install -y gnupg scdaemon yubikey-manager
```

#### 2. Config

```sh
mkdir -p "$HOME/.gnupg"
chmod 700 "$HOME/.gnupg"

cat >"$HOME/.gnupg/gpg.conf" <<EOF
keyid-format 0xlong
with-subkey-fingerprints
personal-cipher-preferences AES256 AES192 AES
personal-digest-preferences SHA512 SHA384 SHA256
personal-compress-preferences ZLIB BZIP2 ZIP Uncompressed
default-preference-list SHA512 SHA384 SHA256 AES256 AES192 AES ZLIB BZIP2 ZIP Uncompressed
cipher-algo AES256
cert-digest-algo SHA512
s2k-cipher-algo AES256
s2k-digest-algo SHA512
keyserver hkps://keys.openpgp.org
display-charset utf-8
EOF

cat >"$HOME/.gnupg/gpg-agent.conf" <<EOF
enable-ssh-support
default-cache-ttl 60
max-cache-ttl 120
# Mitigate slow gpg-agent startup
# https://superuser.com/a/1792323
no-allow-external-cache
EOF

cat >>"$HOME/.profile" <<'EOF'

export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
EOF

cat >>"$HOME/.bashrc" <<'EOF'

export GPG_TTY=$(tty)
EOF

mkdir -p "$HOME/.ssh"
chmod 700 "$HOME/.ssh"

cat >>"$HOME/.ssh/config" <<'EOF'

Match host * exec "gpg-connect-agent updatestartuptty /bye"
EOF

. "$HOME/.profile"
. "$HOME/.bashrc"
gpgconf --kill gpg-agent
gpg-agent
```

#### 3. Import Keys

Insert the YubiKey. Then:

```sh
gpg --edit-card
```

| Prompt    | Answer |
| --------- | ------ |
| gpg/card> | fetch  |
| gpg/card> | quit   |

#### 4. Trust Keys

```sh
gpg --edit-key $KEY_ID
```

| Prompt       | Answer |
| ------------ | ------ |
| gpg>         | trust  |
| ...decision? | 5      |
| ... (y/N)    | y      |
| gpg>         | save   |

### Typical Usage

#### List GnuPG Public Keys

```sh
gpg -k
```

#### List GnuPG Secret Keys

```sh
gpg -K
```

#### List SSH Keys

```sh
ssh-add -L
```

#### Switch to Another Yubikey

```sh
# Remove Yubikey
gpgconf --kill gpg-agent
gpg-agent
gpg-connect-agent "scd serialno" "learn --force" /bye
```

### Miscellaneous

#### Send GnuPG Public Key to Key Server

```sh
gpg --keyserver hkps://keys.openpgp.org --send $KEY_ID
```

#### Add Existing File-based SSH Key

```
ssh-add <path-to-private-key>
```
