# Jāgaro's GnuPG / YubiKey / SSH Guide

The following is a brief, simplified guide to creating non-exportable
[GnuPG](https://gnupg.org/) and [SSH](https://www.openssh.com/) keys that can
live on a [YubiKeys](https://www.yubico.com/). This is based on
[drduh's](https://github.com/drduh) excellent [Guide to using YubiKey for GPG
and SSH](https://github.com/drduh/YubiKey-Guide).

## Motivation

- Easy, portable usage of GnuPG and SSH.
- YubiKeys can hold GnuPG secret keys without exposing them.
- Why not?

## Guides

- [Create Master GnuPG Keys](master.md)
- [Copy GnuPG Sub-Keys to YubiKeys](yubikeys.md)
- [General Usage](usage.md)

## References

- https://github.com/drduh/YubiKey-Guide
- https://www.procustodibus.com/blog/2023/04/how-to-set-up-a-yubikey/
- https://philzimmermann.com/docs/human-oriented-base-32-encoding.txt
