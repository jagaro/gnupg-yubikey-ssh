# YubiKeys with SSH

## Install Requirements

```sh
sudo apt install -y openssh-client ssh-askpass
```

## Setup YubiKey(s)

1.  Insert YubiKey
2.  Run the following:

    ```sh
    ssh-keygen -t ed25519-sk -O resident -O verify-required -N '' \
      -C "my-cool-key" -f "$HOME/.ssh/my-cool-key"
    ```

Repeat the above for each YubiKey.

Note: The private key file (`~/.ssh/my-cool-key` in the above example) does not
contain any private key material.

## Usage

For each client using the key:

```sh
sudo apt install -y openssh-client ssh-askpass
```

Both the private and public key files (`~/.ssh/my-cool-key*` in the above
example) must be copied to the `.ssh` folder.

Add an entry to the identity file:

cat <<EOF >>"$HOME/.ssh/config"

IdentityFile ~/.ssh/my-cool-key
EOF

